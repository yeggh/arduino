#include <Keypad.h>

const int KEY_PAD_ROWS = 4;
const int KEY_PAD_COLS = 3;

byte keys[KEY_PAD_ROWS][KEY_PAD_COLS] = {
  {1, 2, 3},
  {4, 5, 6},
  {7, 8, 9},
  {10, 11, 12}
};



byte rowPins[KEY_PAD_ROWS] = {8, 7, 6, 5}; //connect to the row pinouts of the keypad

byte colPins[KEY_PAD_COLS] = {2, 3, 4}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, KEY_PAD_ROWS, KEY_PAD_COLS );

void setup(){
  Serial.begin(9600);
}


void loop(){  
  handle_keys();
}


int press_counter = 0;
unsigned int press_start_time = 0;
const unsigned int PRESS_TIME_LIMIT = 1000;
int last_key = 0; // if zero then the key pressing has not started yet

String button_strings[12] = {
  "1", "2ABC", "3DEF", "4GHI", "5JKL", "6MNO", "7PQRS", "8TUV", "9WXYZ", "*", "0", "#"
};

// You can edit here:
void output_character(int key, int counter, bool is_real) {
  Serial.println(button_strings[key-1][counter % button_strings[key-1].length()]);
  if (is_real) {
    Serial.println("real!");
    last_key = 0;
    press_counter = 0;
  }
}

void handle_keys() {
  int key = keypad.getKey();
  if (key != NO_KEY) {
    if (key == 1 || key > 9) {
      output_character(key, 0, true); 
    } else {
      if (last_key != 0 && key != last_key) {
        output_character(last_key, press_counter, true);
      } else if (key == last_key) {
        press_counter++;
      }
      press_start_time = millis();
      last_key = key;
      output_character(key, press_counter, false);
    }
  }
  if (millis() - press_start_time > PRESS_TIME_LIMIT && last_key != 0) {
    output_character(last_key, press_counter, true);
  }
}
